[![Build Status](https://magnum.travis-ci.com/doyonghoon/great-coin.svg?token=AHit8yHTbsokqUvD6zf9&branch=modularized-api)](https://magnum.travis-ci.com/doyonghoon/great-coin)

# great-coin
bitstamp 시장에서 코인을 사고 파는 봇.

## 실제 거래
`ExchangeBot` 을 상속한 클래스를 생성하고, `ExchangeBot.start()` 을 호출하면 살행이 됨.

## 모의 거래
시뮬레이팅을 할 때는 거래 정보가 덤프로 제공 되어야 함.
```java
AlgorithmComparator comparator = new AlgorithmComparator();
comparator.compare(
    new PPOAlgorithm(new SimulatingAccount(1000, 10)),
    new BinaryAlgorithm(new SimulatingAccount(1000, 10))
);
```

#### Logs
```
Results for PPOAlgorithm:
	Overall earnings: $201.1790130292784
	Account infos: ExchangeAccount [initialUsdBalance=1000.0, initialBtcBalance=10.0, currentUsdBalance=4018.46, currentBtcBalance=1.3580337822984, feeBalance=419.08, tradeCounter=260]
Results for BinaryAlgorithm:
	Overall earnings: $-1445.991134738302
	Account infos: ExchangeAccount [initialUsdBalance=1000.0, initialBtcBalance=10.0, currentUsdBalance=1192.65, currentBtcBalance=4.973493451723, feeBalance=382.83, tradeCounter=252]
```

## 차트
거래 기록들을 그래프로 보여주는 도구.
```java
ComparativeChart.addTradeSeries("series1", SimulatingMarket.getAllTrades());
ComparativeChart.addTradeSeries("series2", SimulatingMarket.getAllTrades());
ComparativeChart.show();
```

## ExchangeBot
시장에서 실제로 거래하고 싶은 봇을 만들 때 상속받고 만들면 됨.
#### getExchange()
시장의 정보를 받아올 수 있는 객체인데, 이게 있어야 거래정보도 받아올 수 있고 거래(buy/sell)를 진행할 수 있음. 아래처럼 정의하고 리턴해주면 됨.
```java
  @Override public Exchange getExchange() {
    if (mExchange == null) {
      ExchangeSpecification sp = new ExchangeSpecification(BitstampExchange.class.getName());
      sp.setApiKey(System.getenv("BITSTAMP_API_KEY"));
      sp.setSecretKey(System.getenv("BITSTAMP_SECRET_KEY"));
      sp.setPassword(System.getenv("BITSTAMP_PASSWORD"));
      sp.setUserName(System.getenv("BITSTAMP_USERNAME"));
      mExchange = new BitstampExchange();
      mExchange.applySpecification(sp);
    }
    return mExchange;
  }
```

#### getAlgorithm()
`ExchangeBot` 은 하나의 알고리즘이 반드시 필요함. 폴링으로 거래정보가 콜백될 때, 알고리즘으로 지금 거래를 진행할지 말지를 결정함.
```java
  @Override public TradingAlgorithm getAlgorithm() {
    if (mAlgorithm == null) {
      mAlgorithm = new PPOAlgorithm();
    }
    return mAlgorithm;
  }
```

## ExchangeMarket
주로 알고리즘에 사용되는 거래 기록들을 관리하고 필요할 때 알맞는 양식으로 리턴해줌. 시장에서 거래 정보를 가져오기 때문에, 경우에 따라서 별도의 lifecycle 이 존재할 수도 있음. 가령 `BitstampBot` 은 `BitstampMarket.OnBitstampTradeAlertListener` 를 기준으로 알고리즘을 실행하는데, `BitstampMarket` 은 매 8초마다 시장에서 거래정보를 가져오고, 만약 새로운 거래가 있을 때 `BitstampMarket.OnBitstampTradeAlertListener` 으로 콜백을 해줌. 다른 예로, `ExchangeMarket` 을 상속받은 `SimulatingMarket` 은 콜백을 정의 시켜놓지는 않았음. `SimulatingMarket.getAllTrades()` 로 거래 정보들을 리턴해줌.

## ExchangeAccount
시장에서 거래를 진행하거나, 현재 잔고 정보를 가져올 수 있는 객체.

## TradingAlgorithm
봇이 거래를할지 말지 결정해주는 객체. 알고리즘은 반드시 `Order placeOrder()` 를 리턴해주어야 봇이 거래를 진행할 수 있음. 만약 리턴 값이 null 이면 봇은 거래를 하지 않음.

