package com.spencerdo.bitraac.data;

import com.google.gson.annotations.SerializedName;
import com.xeiam.xchange.dto.trade.LimitOrder;
import java.math.BigDecimal;

/**
 * Created by doyonghoon on 15. 3. 11..
 */
public class LocalOrder {

  @SerializedName("order") private LimitOrder mOrder;
  @SerializedName("algorithm") private String mAlgorithmName;
  @SerializedName("algorithm_rate") private double mAlgorithmRate;
  @SerializedName("current_usd") private BigDecimal mCurrentUsdBalance;
  @SerializedName("current_btc") private BigDecimal mCurrentBtcBalance;

  public LocalOrder(LimitOrder order, double algorithmRate, String algorithmName) {
    mOrder = order;
    mAlgorithmRate = algorithmRate;
    mAlgorithmName = algorithmName;
  }

  public void setCurrentUsdBalance(BigDecimal usd) {
    mCurrentUsdBalance = usd;
  }

  public void setCurrentBtcBalance(BigDecimal btc) {
    mCurrentBtcBalance = btc;
  }

  public BigDecimal getCurrentUsdBalance() {
    return mCurrentUsdBalance;
  }

  public BigDecimal getCurrentBtcBalance() {
    return mCurrentBtcBalance;
  }

  public double getAlgorithmRate() {
    return mAlgorithmRate;
  }

  public String getAlgorithmName() {
    return mAlgorithmName;
  }

  public LimitOrder getLimitOrder() {
    return mOrder;
  }
}
