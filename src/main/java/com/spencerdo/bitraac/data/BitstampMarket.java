package com.spencerdo.bitraac.data;

import com.spencerdo.bitraac.algorithm.MarketTradeRemover;
import com.spencerdo.bitraac.algorithm.PollingBitstampMarketDataService;
import com.spencerdo.bitraac.algorithm.WLog;
import com.xeiam.xchange.currency.CurrencyPair;
import com.xeiam.xchange.dto.marketdata.Trade;
import com.xeiam.xchange.dto.marketdata.Trades;
import com.xeiam.xchange.service.polling.marketdata.PollingMarketDataService;
import java.io.IOException;
import java.util.List;

/**
 * Provides Trades and Periods that are specifically related with trading algorithm.
 * @see PollingBitstampMarketDataService.OnPollingMarketDataListener#onUpdateTrade(List)
 *
 * There is a listener that notifies when another opportunity has been created to trade.
 * @see OnBitstampTradeAlertListener#onBitstampTrade(Trade)
 *
 * @author doyonghoon
 */
public class BitstampMarket extends ExchangeMarket implements
    PollingBitstampMarketDataService.OnPollingMarketDataListener {

  public interface OnBitstampTradeAlertListener {
    void onBitstampTrade(Trade lastTrade);
  }

  private final PollingBitstampMarketDataService mPollingService;
  private final OnBitstampTradeAlertListener mCallback;

  /**
   * @param service is to load the latest data from the market.
   * @param tradeCallback will notify when you obtain an another opportunity to trade.
   * */
  public BitstampMarket(PollingMarketDataService service, OnBitstampTradeAlertListener tradeCallback) {
    mCallback = tradeCallback;
    mPollingService = new PollingBitstampMarketDataService(service, this);

    removeAgedTrades();
    startMarketDataRemoveScheduler();
    initTrades(service);
  }

  /**
   * The data that would be used in trading algorithm should be swiped out since
   * the storage for database we use is limited. It will delete all the rows
   * where matches the data has been since three hours and finally prints a log how many rows were deleted.
   *
   * @see DataUtils#removeTradeAfterThreeHours()
   * */
  private void removeAgedTrades() {
    int removedItems = DataUtils.removeTradeAfterThreeHours();
    if (removedItems > 0) {
      WLog.i("removed trade items: " + removedItems + ", reset periods.");
    }
  }

  /**
   * A worker that deletes trades at every fixed time, where matches the trades have been past three hours
   * as mentioned in #removeAgedTrades(). The only difference is that executing in a main thread or in another thread.
   *
   * @see #removeAgedTrades()
   * */
  private void startMarketDataRemoveScheduler() {
    new MarketTradeRemover().start();
  }

  /**
   * Loads data from the market and store it in the memory and persistent storage.
   * */
  private void initTrades(PollingMarketDataService service) {
    try {
      Trades ts = service.getTrades(CurrencyPair.BTC_USD);
      List<Trade> trades = ts.getTrades();
      DataUtils.addPreviousTrades(trades);
      for (Trade t : DataUtils.getLocalTrades()) {
        ExchangeMarket.addTrade(t);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Starts a worker that loads data from the market at every fixed time. This is similar with initTrades(), however
   * it only callbacks when the trades have been loaded from the market.
   *
   * @see #initTrades(PollingMarketDataService)
   * */
  public void startPolling() {
    mPollingService.start();
  }

  @Override public void onUpdateTrade(List<Trade> trades) throws IOException {
    if (trades != null && !trades.isEmpty()) {
      WLog.i("trades: " + trades);
      List<Trade> addedTrades = DataUtils.addPreviousTrades(trades);
      if (!addedTrades.isEmpty()) {
        for (Trade t : addedTrades) {
          ExchangeMarket.addTrade(t);
        }
        Trade lastTrade = DataUtils.getLastLocalTrade();
        if (mCallback != null) {
          // callbacks to the place where algorithm could be run.
          mCallback.onBitstampTrade(lastTrade);
        }
      }
    }
  }
}
