package com.spencerdo.bitraac.data;

import com.xeiam.xchange.dto.Order;
import com.xeiam.xchange.dto.marketdata.Trade;
import java.math.BigDecimal;

/**
 * An exchange account.
 */
public abstract class ExchangeAccount {

  public abstract BigDecimal getCurrentUsdBalance();

  public abstract BigDecimal getCurrentBtcBalance();

  public abstract BigDecimal getFeeRate();

  public abstract boolean isEnoughUsd(Order order, Trade lastTrade);

  public abstract boolean isEnoughBtc(Order order);

  public abstract void buy(BigDecimal amount, BigDecimal price);

  public abstract void sell(BigDecimal amount, BigDecimal price);
}
