package com.spencerdo.bitraac.data;

import com.xeiam.xchange.dto.marketdata.Trade;
import java.util.ArrayList;

/**
 * An exchange market (e.g. Bitstamp, Mt.Gox, BTC-e, etc.)
 */
public class ExchangeMarket {

  /** The list of past periods */
  private static final ArrayList<Period> PREVIOUS_PERIODS = new ArrayList<Period>(100);

  /**
   * @param trade the trade to be added
   */
  public static void addTrade(Trade trade) {
    if (PREVIOUS_PERIODS.isEmpty()) {
      // First trade
      PREVIOUS_PERIODS.add(new Period(trade));
    } else {
      // Subsequent trades
      Period lastPeriod = PREVIOUS_PERIODS.get(PREVIOUS_PERIODS.size() - 1);
      while (!lastPeriod.inPeriod(trade.getTimestamp())) {
        PREVIOUS_PERIODS.add(new Period(lastPeriod.getEndTimestamp()));
        lastPeriod = PREVIOUS_PERIODS.get(PREVIOUS_PERIODS.size() - 1);
      }
      lastPeriod.addTrade(trade);
    }
  }

  public static void clearTrade() {
    PREVIOUS_PERIODS.clear();
  }

  /**
   * @param nbPeriods the number of periods
   * @return true if there is at least nbPeriods periods, false otherwise
   */
  public static boolean isEnoughPeriods(int nbPeriods) {
    return (PREVIOUS_PERIODS.size() >= nbPeriods);
  }

  /**
   * @param nbTrades the number of trades
   * @return true if the current period contains at least nbTrades trades, false otherwise
   */
  public static boolean isEnoughTrades(int nbTrades) {
    return !PREVIOUS_PERIODS.isEmpty() && (PREVIOUS_PERIODS.get(PREVIOUS_PERIODS.size() - 1)
        .getTrades()
        .size() >= nbTrades);
  }

  public static void clearPreviousPeriods() {
    PREVIOUS_PERIODS.clear();
  }

  /**
   * @return the list of past periods
   */
  public static ArrayList<Period> getPreviousPeriods() {
    return PREVIOUS_PERIODS;
  }

  /**
   * @return the past trades for the current period
   */
  public static ArrayList<Trade> getPreviousTrades() {
    return PREVIOUS_PERIODS.get(PREVIOUS_PERIODS.size() - 1).getTrades();
  }
}
