package com.spencerdo.bitraac.bot;

import com.spencerdo.bitraac.data.ExchangeAccount;
import com.xeiam.xchange.dto.Order;
import com.xeiam.xchange.dto.marketdata.Trade;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * The account that would be used in simulated trading.
 *
 * @author doyonghoon
 */
public class SimulatingAccount extends ExchangeAccount {

  private BigDecimal initialUsdBalance;
  private BigDecimal initialBtcBalance;

  private BigDecimal currentUsdBalance;
  private BigDecimal currentBtcBalance;

  private BigDecimal feeBalance = BigDecimal.ZERO;

  private int tradeCounter = 0;

  /**
   * @param initialUsdBalance the initial USD balance
   * @param initialBtcBalance the initial BTC balance
   */
  public SimulatingAccount(double initialUsdBalance, double initialBtcBalance) {
    this(new BigDecimal(initialUsdBalance), new BigDecimal(initialBtcBalance));
  }

  /**
   * @param initialUsdBalance the initial USD balance
   * @param initialBtcBalance the initial BTC balance
   */
  public SimulatingAccount(BigDecimal initialUsdBalance, BigDecimal initialBtcBalance) {
    this.initialUsdBalance = initialUsdBalance;
    this.initialBtcBalance = initialBtcBalance;
    this.currentUsdBalance = initialUsdBalance;
    this.currentBtcBalance = initialBtcBalance;
  }

  @Override public BigDecimal getCurrentUsdBalance() {
    return currentUsdBalance;
  }

  @Override public BigDecimal getCurrentBtcBalance() {
    return currentBtcBalance;
  }

  @Override public BigDecimal getFeeRate() {
    return new BigDecimal(".0025");
  }

  @Override public boolean isEnoughUsd(Order order, Trade lastTrade) {
    return order.getType() != Order.OrderType.BID || (order.getTradableAmount()
        .multiply(lastTrade.getPrice())
        .compareTo(currentUsdBalance) <= 0);
  }

  @Override public boolean isEnoughBtc(Order order) {
    return order.getType() != Order.OrderType.ASK || (order.getTradableAmount()
        .compareTo(currentBtcBalance) <= 0);
  }

  @Override public void buy(BigDecimal amount, BigDecimal price) {
    // Deducting transaction fee
    BigDecimal usdAmount = deductFee(amount.multiply(price));
    currentUsdBalance = currentUsdBalance.subtract(usdAmount);
    currentBtcBalance = currentBtcBalance.add(usdAmount.divide(price, RoundingMode.HALF_UP));
    // Updating the trade counter
    tradeCounter++;
  }

  @Override public void sell(BigDecimal amount, BigDecimal price) {
    currentBtcBalance = currentBtcBalance.subtract(amount);
    // Deducting transaction fee
    BigDecimal usdAmount = deductFee(amount.multiply(price));
    currentUsdBalance = currentUsdBalance.add(usdAmount);
    // Updating the trade counter
    tradeCounter++;
  }

  /**
   * @param currentBtcUsd the current BTC/USD rate
   * @return the overall earnings (can be negative in case of loss) in USD
   */
  public double getOverallEarnings(BigDecimal currentBtcUsd) {
    BigDecimal usdDifference = currentUsdBalance.subtract(initialUsdBalance);
    BigDecimal btcDifference = currentBtcBalance.subtract(initialBtcBalance);
    return usdDifference.add(btcDifference.multiply(currentBtcUsd)).doubleValue();
  }

  /**
   * @return the overall deducted fees in USD
   */
  public double getOverallDeductedFees() {
    return feeBalance.doubleValue();
  }

  /**
   * @param amount the USD amount before fee deduction
   * @return the USD amount after fee deduction
   */
  private BigDecimal deductFee(BigDecimal amount) {
    BigDecimal feeAmount = amount.multiply(getFeeRate()).setScale(2, BigDecimal.ROUND_UP);
    feeBalance = feeBalance.add(feeAmount);
    return amount.subtract(feeAmount);
  }

  @Override
  public String toString() {
    return "ExchangeAccount [initialUsdBalance=" + initialUsdBalance.doubleValue()
        + ", initialBtcBalance=" + initialBtcBalance.doubleValue()
        + ", currentUsdBalance=" + currentUsdBalance.doubleValue()
        + ", currentBtcBalance=" + currentBtcBalance.doubleValue()
        + ", feeBalance=" + feeBalance.doubleValue()
        + ", tradeCounter=" + tradeCounter
        + "]";
  }
}
