package com.spencerdo.bitraac.bot;

import com.spencerdo.bitraac.algorithm.WLog;
import com.spencerdo.bitraac.data.DataUtils;
import com.spencerdo.bitraac.data.ExchangeAccount;
import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.currency.CurrencyPair;
import com.xeiam.xchange.dto.Order;
import com.xeiam.xchange.dto.account.AccountInfo;
import com.xeiam.xchange.dto.marketdata.Trade;
import com.xeiam.xchange.dto.trade.LimitOrder;
import com.xeiam.xchange.exceptions.ExchangeException;
import com.xeiam.xchange.exceptions.NotAvailableFromExchangeException;
import com.xeiam.xchange.exceptions.NotYetImplementedForExchangeException;
import com.xeiam.xchange.service.polling.trade.PollingTradeService;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * The account that is able to create a trade in the security.
 * It also provides infomation about the balance of an account in the market and a trading fee rate.
 *
 * @see #buy(BigDecimal, BigDecimal)
 * @see #sell(BigDecimal, BigDecimal)
 * @see #getCurrentBtcBalance()
 * @see #getCurrentUsdBalance()
 * @see #getFeeRate()
 *
 * @author doyonghoon
 */
public class BitstampAccount extends ExchangeAccount {

  public interface TradeCallback {
    void onTradeDone(String orderId, LimitOrder order, ExchangeAccount account);
  }

  private PollingTradeService mTradeService;
  private AccountInfo mAccountInfo;
  private final TradeCallback mCallback;

  public BitstampAccount(Exchange exchange, TradeCallback callback) {
    mCallback = callback;
    try {
      mAccountInfo = exchange.getPollingAccountService().getAccountInfo();
      mTradeService = exchange.getPollingTradeService();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * @return the current USD balance
   */
  @Override public BigDecimal getCurrentUsdBalance() {
    BigDecimal result = new BigDecimal(mAccountInfo.getBalance("USD").doubleValue()).subtract(new BigDecimal("1414"));
//    return new BigDecimal(mAccountInfo.getBalance("USD").doubleValue());
    return result;
  }

  /**
   * @return the current BTC balance
   */
  @Override public BigDecimal getCurrentBtcBalance() {
    BigDecimal customizedBalance = mAccountInfo.getBalance("BTC").subtract(new BigDecimal("19.8"));
//    return new BigDecimal(mAccountInfo.getBalance("BTC").doubleValue());
    return customizedBalance;
  }

  public BigDecimal getFeeRate() {
    return mAccountInfo.getTradingFee();
  }

  /**
   * @param order the order to be placed
   * @param lastTrade the last trade data
   * @return true if there is enough money to place the order, false otherwise
   */
  public boolean isEnoughUsd(Order order, Trade lastTrade) {
    return order.getType() != Order.OrderType.BID || (order.getTradableAmount()
        .multiply(lastTrade.getPrice())
        .compareTo(getCurrentUsdBalance()) <= 0);
  }

  /**
   * @param order the order to be placed
   * @return true if there is enough bitcoins to place the order, false otherwise
   */
  public boolean isEnoughBtc(Order order) {
    return order.getType() != Order.OrderType.ASK || (order.getTradableAmount().compareTo(
        getCurrentBtcBalance()) <= 0);
  }

  /**
   * Buy an amount of BTC.
   * (Deduct the exchange transaction fee so the real amount of buyed BTC will be lower)
   *
   * @param amount the amount of BTC to buy
   * @param price the unit price
   */
  @Override public void buy(BigDecimal amount, BigDecimal price) {
    LimitOrder limitOrder = createLimitOrder(Order.OrderType.BID, amount, price);
    try {
      requestLimitOrder(limitOrder, mCallback);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Sells an amount of BTC.
   * (Deduct the exchange transaction fee)
   *
   * @param amount the amount of BTC to sell
   * @param price the unit price
   */
  public void sell(BigDecimal amount, BigDecimal price) {
    LimitOrder limitOrder = createLimitOrder(Order.OrderType.ASK, amount, price);
    try {
      requestLimitOrder(limitOrder, mCallback);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private LimitOrder createLimitOrder(Order.OrderType type, BigDecimal amount, BigDecimal price) {
    return new LimitOrder(type, amount, CurrencyPair.BTC_USD, "", null, price);
  }

  /**
   * Places an order and notifies to the place where made the order.
   *
   * @param limitOrder that is desired to be placed.
   * @param callback notifies when the order has been successfully placed.
   * */
  private void requestLimitOrder(LimitOrder limitOrder, TradeCallback callback) throws IOException {
    cancelOrderIfAlreadyCreatedBefore(limitOrder.getType());

    WLog.i("request order: " + limitOrder);
    String limitOrderId = null;
    try {
      limitOrderId = mTradeService.placeLimitOrder(limitOrder);
      WLog.i("response limitOrderId: " + limitOrderId);
    } catch (NotAvailableFromExchangeException e) {
      e.printStackTrace();
    } catch (NotYetImplementedForExchangeException e) {
      e.printStackTrace();
    } catch (ExchangeException e) {
      e.printStackTrace();
    }

    if (callback != null && limitOrderId != null) {
      callback.onTradeDone(limitOrderId, limitOrder, this);
    } else {
      WLog.i("failed to order: " + limitOrder);
    }
  }

  /**
   * Cancels same type of open orders that have been made before.
   * This will be triggered right before creating another order.
   * @see #requestLimitOrder(LimitOrder, TradeCallback)
   *
   * @param orderType that desired to be removed in the market.
   *
   * */
  private void cancelOrderIfAlreadyCreatedBefore(Order.OrderType orderType) throws IOException {
    List<LimitOrder> orders = mTradeService.getOpenOrders().getOpenOrders();
    if (orders != null && orders.size() > 0) {
      WLog.i("open orders: " + orders);
      for (LimitOrder o : orders) {
        if (o.getType() == orderType) {
          boolean canceled = mTradeService.cancelOrder(o.getId());
          if (canceled && DataUtils.removeLocalOrder(o.getId())) {
            WLog.i("order: " + o.getType().name() + "/" + o.getId() + " canceled.");
          }
        }
      }
    }
  }
}
