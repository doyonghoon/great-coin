package com.spencerdo.bitraac.bot;

import au.com.bytecode.opencsv.CSVReader;
import com.spencerdo.bitraac.AlgorithmComparator;
import com.spencerdo.bitraac.data.ExchangeMarket;
import com.xeiam.xchange.currency.CurrencyPair;
import com.xeiam.xchange.dto.marketdata.Trade;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by doyonghoon on 15. 5. 3..
 */
public class SimulatingMarket extends ExchangeMarket {

  /** The list of all trades */
  private static ArrayList<Trade> TRADES = null;

  /**
   * @return the trades
   */
  public static ArrayList<Trade> getAllTrades(String resourceName) {
    if (TRADES == null) {
      TRADES = getLocalTrades(resourceName);
    }
    return TRADES;
  }

  /**
  * (Keeps only the last 7 days.)
  * @return the local (i.e. CSV-based) trades
  */
  private static ArrayList<Trade> getLocalTrades(String resourceName) {
      ArrayList<Trade> trades = new ArrayList<Trade>();
      try {
          BufferedReader fileReader = new BufferedReader(new InputStreamReader(AlgorithmComparator.class.getClassLoader().getResourceAsStream("tradeDumps/" + resourceName)));
          CSVReader csvReader = new CSVReader(fileReader, ',');
          String[] line;
          while ((line = csvReader.readNext()) != null) {
              Date timestamp = new Date(Long.parseLong(line[0]) * 1000);
            BigDecimal price = new BigDecimal(line[1]);
              BigDecimal tradableAmount = new BigDecimal(line[2]);
              Trade trade = new Trade(null, tradableAmount, CurrencyPair.BTC_USD, price, timestamp, "0");
              trades.add(trade);
          }
      } catch (IOException ioe) {
        Logger.getLogger(AlgorithmComparator.class.getName()).log(Level.SEVERE, "Unable to load trades from CSV", ioe);
      }

      if (!trades.isEmpty()) {
          // /!\ Performance patch /!\
          // Only keeping the last 7 days
          Trade lastTrade = trades.get(trades.size()-1);
          Date firstDateKept = new Date(lastTrade.getTimestamp().getTime() - TimeUnit.DAYS.toMillis(7));
          for (int i = trades.size() - 1; i >= 0; i--) {
              if (trades.get(i).getTimestamp().before(firstDateKept)) {
                  trades.remove(i);
              }
          }
      }
      return trades;
  }
}
