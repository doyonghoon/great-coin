package com.spencerdo.bitraac.bot;

import com.spencerdo.bitraac.algorithm.ExchangeBot;
import com.spencerdo.bitraac.algorithm.GreatAlgorithm1;
import com.spencerdo.bitraac.algorithm.TradingAlgorithm;
import com.spencerdo.bitraac.algorithm.WLog;
import com.spencerdo.bitraac.data.BitstampMarket;
import com.spencerdo.bitraac.data.DataUtils;
import com.spencerdo.bitraac.data.ExchangeAccount;
import com.spencerdo.bitraac.data.LocalOrder;
import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.ExchangeSpecification;
import com.xeiam.xchange.bitstamp.BitstampExchange;
import com.xeiam.xchange.dto.marketdata.Trade;
import com.xeiam.xchange.dto.trade.LimitOrder;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;

/**
 * buy/sell coins in the real market, bitstamp.
 *
 * @author doyonghoon
 */
public class BitstampBot extends ExchangeBot
    implements BitstampMarket.OnBitstampTradeAlertListener, BitstampAccount.TradeCallback {

  private Exchange mExchange = null;
  private BitstampAccount mAccount = null;
  private TradingAlgorithm mAlgorithm = null;

  public BitstampBot() {
    getExchange();
    getExchangeAccount();
    getAlgorithm();
  }

  private void printCurrentStatus(ExchangeAccount account) {
    double currentBitcoin = account.getCurrentBtcBalance().doubleValue();
    double currentUsd = account.getCurrentUsdBalance().doubleValue();
    double fee = account.getFeeRate().doubleValue();
    WLog.i("btc: " + currentBitcoin + ", usd: " + currentUsd + ", fee: " + fee);
  }

  @Override public TradingAlgorithm getAlgorithm() {
    if (mAlgorithm == null) {
      mAlgorithm = new GreatAlgorithm1(getExchangeAccount());
    }
    return mAlgorithm;
  }

  @Override protected ExchangeAccount getExchangeAccount() {
    if (mAccount == null) {
      mAccount = new BitstampAccount(getExchange(), this);
    }
    return mAccount;
  }

  @Override public Exchange getExchange() {
    if (mExchange == null) {
      ExchangeSpecification sp = new ExchangeSpecification(BitstampExchange.class.getName());
      sp.setApiKey(System.getenv("BITSTAMP_API_KEY"));
      sp.setSecretKey(System.getenv("BITSTAMP_SECRET_KEY"));
      sp.setPassword(System.getenv("BITSTAMP_PASSWORD"));
      sp.setUserName(System.getenv("BITSTAMP_USERNAME"));
      mExchange = new BitstampExchange();
      mExchange.applySpecification(sp);
    }
    return mExchange;
  }

  @Override public void onStart() {
    printCurrentStatus(getExchangeAccount());
    new BitstampMarket(getExchange().getPollingMarketDataService(), this).startPolling();
  }

  @Override public void onBitstampTrade(final Trade lastTrade) {
    if (getAlgorithm() != null && lastTrade != null) {
      getAlgorithm().setExchangeAccount(getExchangeAccount());
      getAlgorithm().placeOrder(order -> {
        switch (order.getType()) {
          case BID:
            if (getExchangeAccount().isEnoughUsd(order, lastTrade)) {
              getExchangeAccount().buy(order.getTradableAmount(), lastTrade.getPrice());
            }
            break;
          case ASK:
            if (getExchangeAccount().isEnoughBtc(order)) {
              getExchangeAccount().sell(order.getTradableAmount(), lastTrade.getPrice());
            }
            break;
        }
      }, lastTrade.getPrice());
    }
  }

  @Override public void onTradeDone(String orderId, LimitOrder order, ExchangeAccount account) {
    if (order != null && !StringUtils.isEmpty(orderId)) {
      LimitOrder completeOrder = LimitOrder.Builder.from(order).id(orderId).timestamp(new Date()).build();
      LocalOrder localOrder = new LocalOrder(completeOrder, 0, "nothing");
      localOrder.setCurrentBtcBalance(account.getCurrentBtcBalance());
      localOrder.setCurrentUsdBalance(account.getCurrentUsdBalance());
      boolean addLocalOpenOrder = DataUtils.addLocalOpenOrder(localOrder);
      if (addLocalOpenOrder) {
        WLog.i("successfully added into local_order orderId: " + orderId);
        switch (localOrder.getLimitOrder().getType()) {
          case ASK:
            DataUtils.removeBidOrderLastRow();
            break;
          case BID:
            DataUtils.addBidOpenOrder(localOrder);
            break;
        }
      } else {
        WLog.i("failed to add order_id: " + orderId);
      }
    } else {
      WLog.i("failed to order: " + order);
    }
  }
}
