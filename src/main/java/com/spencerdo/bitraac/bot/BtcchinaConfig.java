package com.spencerdo.bitraac.bot;

import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.ExchangeFactory;
import com.xeiam.xchange.ExchangeSpecification;
import com.xeiam.xchange.btcchina.BTCChinaExchange;

/**
 * Created by doyonghoon on 15. 3. 9..
 */
public class BtcchinaConfig {
  public static Exchange createExchange() {
    ExchangeSpecification exSpec = new BTCChinaExchange().getDefaultExchangeSpecification();
    exSpec.setUserName(System.getenv("BTCCHINA_USERNAME"));
    exSpec.setPassword(System.getenv("BTCCHINA_PASSWORD"));
    exSpec.setApiKey(System.getenv("BTCCHINA_API_KEY"));
    exSpec.setSecretKey(System.getenv("BTCCHINA_SECRET_KEY"));
    return ExchangeFactory.INSTANCE.createExchange(exSpec);
  }
}
