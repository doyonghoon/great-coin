package com.spencerdo.bitraac.bot;

import com.spencerdo.bitraac.data.Greeting;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import spark.ModelAndView;
import spark.Spark;
import spark.template.freemarker.FreeMarkerEngine;

/**
 * Main class that starts an API server and runs trading bot.
 */
public class Main {
  public static void main(String[] args) {
    startBitstampBot();
    startApiServer();
  }

  private static void startBitstampBot() {
    BitstampBot bot = new BitstampBot();
    bot.onStart();
  }

  private static void startApiServer() {
    FreeMarkerEngine freeMarkerEngine = new FreeMarkerEngine();
    Configuration c = new Configuration();
    c.setTemplateLoader(new ClassTemplateLoader(Greeting.class, "/"));
    freeMarkerEngine.setConfiguration(c);
    Spark.setPort(Integer.parseInt(System.getenv("PORT")));
    Spark.staticFileLocation("/assets");
    Spark.get("/hello", (request, response) -> "hello world!");
    Spark.get("/", (req, res) -> {
      res.status(200);
      res.type("text/html");
      return freeMarkerEngine.render(new ModelAndView(null, "assets/posts.ftl"));
    });
  }
}
