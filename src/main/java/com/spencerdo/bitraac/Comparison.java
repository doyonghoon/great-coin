package com.spencerdo.bitraac;

import com.spencerdo.bitraac.algorithm.GreatAlgorithm1;
import com.spencerdo.bitraac.algorithm.TradingAlgorithm;
import com.spencerdo.bitraac.algorithm.WLog;
import com.spencerdo.bitraac.bot.SimulatingAccount;
import com.spencerdo.bitraac.bot.SimulatingMarket;

/**
 * simulates trading
 */
public class Comparison {

  private static final int DUMP_NUMBER = 20;

  public static void main(String[] args) {
    ComparativeChart.addTradeSeries(getBitstampDumpName(DUMP_NUMBER), SimulatingMarket.getAllTrades(getBitstampDumpName(DUMP_NUMBER)));
    ComparativeChart.show();
    AlgorithmComparator comparator = new AlgorithmComparator(getBitstampDumpName(DUMP_NUMBER));
    comparator.compare(new GreatAlgorithm1(new SimulatingAccount(0, 40)));
  }

  private static String getBitstampDumpName(int n) {
    return "bitstamp_usd." + n + ".csv";
  }

  private static void compare(int offset, int max, TradingAlgorithm... algorithms) {
    for (int i = offset; i <= max; i++) {
      WLog.i("compare dump: " + i);
      AlgorithmComparator comparator = new AlgorithmComparator("bitstamp_usd." + i + ".csv");
      comparator.compare(algorithms);
    }
  }
}
