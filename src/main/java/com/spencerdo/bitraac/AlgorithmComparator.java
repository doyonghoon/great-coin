package com.spencerdo.bitraac;

import com.spencerdo.bitraac.algorithm.TradingAlgorithm;
import com.spencerdo.bitraac.bot.SimulatingAccount;
import com.spencerdo.bitraac.bot.SimulatingMarket;
import com.spencerdo.bitraac.data.ExchangeAccount;
import com.spencerdo.bitraac.data.ExchangeMarket;
import com.xeiam.xchange.dto.marketdata.Trade;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AlgorithmComparator {

  private final List<Trade> mAllTrades = new ArrayList<Trade>();

  public AlgorithmComparator(String dumpFileName) {
    this(SimulatingMarket.getAllTrades(dumpFileName));
  }

  public AlgorithmComparator(List<Trade> trades) {
    //ExchangeMarket.clearTrade();
    mAllTrades.addAll(trades);
  }

  /**
   * @param algorithms the algorithms to be compared
   */
  public void compare(TradingAlgorithm... algorithms) {
    // Processing orders
    BigDecimal btcUsd = null;
    for (Trade trade : mAllTrades) {
      ExchangeMarket.addTrade(trade);
      for (TradingAlgorithm algorithm : algorithms) {
        processMarketOrder(algorithm, trade);
      }
      btcUsd = trade.getPrice();
    }

    // Results
    for (TradingAlgorithm algorithm : algorithms) {
      ExchangeAccount account = algorithm.getExchangeAccount();
      if (account instanceof SimulatingAccount) {
        SimulatingAccount simulatingAccount = (SimulatingAccount) account;
        System.out.println("Results for "
            + algorithm.getClass().getSimpleName()
            + ":"
            + "\n\tOverall earnings: $"
            + simulatingAccount.getOverallEarnings(btcUsd)
            + "\n\tAccount infos: "
            + account);
      }
    }
  }

  /**
   * Process a market order.
   *
   * @param algorithm the trading algorithm
   * @param lastTrade the last trade data
   */
  private void processMarketOrder(final TradingAlgorithm algorithm, final Trade lastTrade) {
    if (algorithm != null) {
      algorithm.placeOrder(order -> {
        ExchangeAccount account = algorithm.getExchangeAccount();
        switch (order.getType()) {
          case BID:
            if (account.isEnoughUsd(order, lastTrade)) {
              account.buy(order.getTradableAmount(), lastTrade.getPrice());
            }
            break;
          case ASK:
            if (account.isEnoughBtc(order)) {
              account.sell(order.getTradableAmount(), lastTrade.getPrice());
            }
            break;
        }
      }, lastTrade.getPrice());
    }
  }
}
