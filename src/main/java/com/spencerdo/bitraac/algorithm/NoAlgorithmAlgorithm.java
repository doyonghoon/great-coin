package com.spencerdo.bitraac.algorithm;

import com.spencerdo.bitraac.data.ExchangeAccount;
import com.xeiam.xchange.dto.Order;
import rx.functions.Action1;

import java.math.BigDecimal;

public class NoAlgorithmAlgorithm extends TradingAlgorithm {

  public NoAlgorithmAlgorithm(ExchangeAccount account) {
    super(account);
  }

  @Override
  public void placeOrder(Action1<Order> orderAction, BigDecimal lastPrice) {

  }

//  @Override public Order placeOrder() {
//    Order order = null;
//    if (ExchangeMarket.isEnoughTrades(1)) {
//      List<Trade> pastTrades = ExchangeMarket.getPreviousTrades();
//      Trade lastTrade = pastTrades.get(pastTrades.size() - 1);
//      BigDecimal nbBtcToBuy = new BigDecimal(1000.0 / lastTrade.getPrice().doubleValue());
//      order = new MarketOrder(Order.OrderType.BID, nbBtcToBuy, CurrencyPair.BTC_USD);
//    }
//    return order;
//  }
}
