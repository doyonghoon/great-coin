package com.spencerdo.bitraac.algorithm;

import com.spencerdo.bitraac.algorithm.phase.Phase1;
import com.spencerdo.bitraac.data.ExchangeAccount;
import com.spencerdo.bitraac.data.ExchangeMarket;
import com.xeiam.xchange.dto.Order;
import rx.functions.Action1;

import java.math.BigDecimal;

/**
 * It uses various indicators to define a current market trending, and
 * offers an opportunity to create a trade.
 *
 * MACD is the key indicator to see the current trend is uptrend or downtrend in long term
 * perspective.
 *
 * @author doyonghoon
 */
public class GreatAlgorithm1 extends TradingAlgorithm {

  private Phase1 mPhase1 = new Phase1();

  public GreatAlgorithm1(ExchangeAccount account) {
    super(account);
  }

  @Override public void placeOrder(Action1<Order> orderAction, BigDecimal lastPrice) {
    if (ExchangeMarket.isEnoughPeriods(Phase1.LONG_TERM_PERIOD)) {
      mPhase1.execute(orderAction, getExchangeAccount(), lastPrice);
    }
  }
}
