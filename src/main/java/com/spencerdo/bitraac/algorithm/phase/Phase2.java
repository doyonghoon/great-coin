package com.spencerdo.bitraac.algorithm.phase;

import com.spencerdo.bitraac.algorithm.indicators.Indicator;
import com.spencerdo.bitraac.algorithm.indicators.RSI;
import com.spencerdo.bitraac.data.ExchangeMarket;
import java.math.BigDecimal;

/**
 * Created by doyonghoon on 15. 5. 9..
 * @author doyonghoon
 */
public class Phase2 implements Indicator<PhaseResult> {

  private static final int SHORT_TERM_PERIOD = 14;
  private Trending mTrending = Trending.NONE;
  private BigDecimal mRsiValue = BigDecimal.ZERO;

  public Phase2() {

  }

  @Override public PhaseResult execute() {
    BigDecimal rsi = new RSI(ExchangeMarket.getPreviousPeriods(), SHORT_TERM_PERIOD).execute();
    if (rsi.compareTo(BigDecimal.ZERO) == 0) {
      return null;
    }
    double diff = rsi.subtract(mRsiValue).doubleValue();
    mRsiValue = rsi;
    if (diff > 2.5) {
      return new PhaseResult(mTrending = Trending.UPTREND, rsi.doubleValue());
    } else if (diff < 2.5) {
      return new PhaseResult(mTrending = Trending.DOWNTREND, rsi.doubleValue());
    }
    return null;
  }
}
