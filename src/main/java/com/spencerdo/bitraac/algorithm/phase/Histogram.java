package com.spencerdo.bitraac.algorithm.phase;

/**
 * A data that holds a direction of current trending and how deep the trending is proceeding.
 *
 * @author doyonghoon
 */
public class Histogram {

  private double mHistogram = 0;
  private Trending mTrending = Trending.NONE;

  public Histogram() {
  }

  public Histogram(double histogram, Trending trending) {
    setHistogram(histogram);
    setTrending(trending);
  }

  public double getHistogram() {
    return mHistogram;
  }

  public Trending getTrending() {
    return mTrending;
  }

  public void setHistogram(double histogram) {
    mHistogram = histogram;
  }

  public void setTrending(Trending trending) {
    mTrending = trending;
  }

  @Override
  public String toString() {
    return "{ trending: " + mTrending.name().toLowerCase() + ", histogram: " + getHistogram() + " }";
  }

  @Override
  public int hashCode() {
    return mTrending.hashCode() + Double.toString(mHistogram).hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj != null && obj instanceof Histogram) {
      Histogram tmp = (Histogram) obj;
      boolean isEqualTrending = tmp.getTrending().compareTo(getTrending()) == 0;
      boolean isEqualHistogram = Double.compare(tmp.getHistogram(), getHistogram()) == 0;
      return isEqualTrending && isEqualHistogram;
    }
    return super.equals(obj);
  }
}
