package com.spencerdo.bitraac.algorithm.phase;

import com.spencerdo.bitraac.algorithm.WLog;
import com.spencerdo.bitraac.algorithm.indicators.MACD;
import com.spencerdo.bitraac.data.ExchangeAccount;
import com.spencerdo.bitraac.data.ExchangeMarket;
import com.xeiam.xchange.currency.CurrencyPair;
import com.xeiam.xchange.dto.Order;
import com.xeiam.xchange.dto.trade.MarketOrder;
import java.math.BigDecimal;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func3;

/**
 * Created by doyonghoon on 15. 5. 9..
 *
 * @author doyonghoon
 */
public class Phase1 {

  private static final short NUMBER_OF_MAX_DECIMAL_PLACES = 8;
  /**
   * Overall earnings: $195.56929122700907 -> 5.0 coins Overall earnings: $225.4748796468931 -> 4.0
   * coins Overall earnings: $197.50813276849124 -> 3.0 coins Overall earnings: $143.4423852052935
   * -> 2.0 coins
   */
  private static final BigDecimal COINS_AMOUNT = new BigDecimal(".5");
  private static final int SHORT_TERM_PERIOD = 12;
  public static final int LONG_TERM_PERIOD = 26;

  public static double round(double value, int places) {
    if (places < 0) {
      throw new IllegalArgumentException();
    }
    long factor = (long) Math.pow(10, places);
    value = value * factor;
    long tmp = Math.round(value);
    return (double) tmp / factor;
  }

  public static BigDecimal getMaxCoins(BigDecimal price, BigDecimal usd) {
    boolean hasEnoughUsd = price.multiply(COINS_AMOUNT).compareTo(usd) < 0;
    if (hasEnoughUsd) {
      return COINS_AMOUNT;
    }
    BigDecimal minCoins = new BigDecimal(5 / price.doubleValue());
    BigDecimal maxCoins = usd.divide(price, BigDecimal.ROUND_HALF_UP);
    maxCoins = maxCoins.subtract(maxCoins.multiply(new BigDecimal(".0025")));
    if (maxCoins.compareTo(minCoins) >= 0) {
      return maxCoins.setScale(NUMBER_OF_MAX_DECIMAL_PLACES, BigDecimal.ROUND_HALF_DOWN);
    }
    return BigDecimal.ZERO;
  }

  private Signal mSignal = new Signal(9); // $19221(9), $18821(7)
  private Histogram mCapturedHistogram = new Histogram();
  private ExchangeAccount mAccount = null;

  private BigDecimal mLastTradePrice = BigDecimal.ZERO;
  private Order.OrderType mLastTradeOrderType = null;

  /**
   * a) crossover means trending starts! swipe out the histograms. b) grab the amount of histogram,
   * and see how high the histogram is! c) alerts when the histogram is at the highest point!
   */
  public void execute(Action1<Order> action, final ExchangeAccount account, final BigDecimal lastPrice) {
    mAccount = account;
    CrossoverObservable crossoverObservable = new CrossoverObservable(mCapturedHistogram, getHistogramValue());
    Observable<Histogram> histogramObservable = Observable.create(crossoverObservable).doOnNext(captureHistogram()).filter(filterHistogram(lastPrice));

    // carefully create an order, based on tradable amount.
    Observable
        .zip(histogramObservable,
            Observable.just(account),
            Observable.just(lastPrice),
            placeOrderIfReady())
        .filter(filterOrder())
        .doOnNext(new Action1<Order>() {
          @Override
          public void call(Order order) {
            mLastTradeOrderType = order.getType();
            mLastTradePrice = lastPrice;
          }
        })
        .subscribe(action);
  }

  private Func3<Histogram, ExchangeAccount, BigDecimal, Order> placeOrderIfReady() {
    return (histogram, exchangeAccount, price) -> {
      switch (histogram.getTrending()) {
        case STRONG_DOWNTREND:
        case DOWNTREND:
          BigDecimal availableCoins1 = getMaxCoins(price, exchangeAccount.getCurrentUsdBalance());
          if (availableCoins1 != null && availableCoins1.compareTo(BigDecimal.ZERO) > 0) {
            return new MarketOrder(Order.OrderType.BID, trimDecimalPlacesIfNeeded(availableCoins1), CurrencyPair.BTC_USD);
          }
          return null;
        case STRONG_UPTREND:
        case UPTREND:
          BigDecimal availableCoins2 = exchangeAccount.getCurrentBtcBalance().compareTo(COINS_AMOUNT) > 0 ? COINS_AMOUNT : exchangeAccount.getCurrentBtcBalance();
          return new MarketOrder(Order.OrderType.ASK, trimDecimalPlacesIfNeeded(availableCoins2), CurrencyPair.BTC_USD);
        default:
          return null;
      }
    };
  }

  public Func1<Histogram, Boolean> filterHistogram(final BigDecimal lastPrice) {
    return histogram -> {
      double absoluteValue = Math.abs(histogram.getHistogram());
      boolean signaled = Double.compare(absoluteValue, 0.07) >= 0;
      if (signaled) {
        WLog.i("signal: " + signaled + ", trend: " + histogram.getTrending().name().toLowerCase() + ", histogram: " +
            round(histogram.getHistogram(), 2) + ", lastPrice: " + round(lastPrice.doubleValue(), 2) + ", currentBalance: " +
            mAccount.getCurrentBtcBalance().setScale(2, BigDecimal.ROUND_HALF_UP) + "/" + mAccount.getCurrentUsdBalance().setScale(2, BigDecimal.ROUND_HALF_UP));
      }
      return signaled;
    };
  }

  private Func1<Order, Boolean> filterOrder() {
    return new Func1<Order, Boolean>() {
      @Override
      public Boolean call(Order order) {
        return order != null && order.getTradableAmount() != null && order.getTradableAmount().compareTo(BigDecimal.ZERO) > 0;
      }
    };
  }

  private Action1<Histogram> captureHistogram() {
    return histogram -> mCapturedHistogram = histogram;
  }

  public BigDecimal trimDecimalPlacesIfNeeded(BigDecimal amount) {
    if (getNumberOfDecimalPlace(amount) > NUMBER_OF_MAX_DECIMAL_PLACES) {
      return amount.setScale(NUMBER_OF_MAX_DECIMAL_PLACES, BigDecimal.ROUND_HALF_DOWN);
    }
    return amount;
  }

  private int getNumberOfDecimalPlace(BigDecimal number) {
    int scale = number.stripTrailingZeros().scale();
    return scale > 0 ? scale : 0;
  }

  private double getHistogramValue() {
    BigDecimal macd = new MACD(ExchangeMarket.getPreviousPeriods(), SHORT_TERM_PERIOD,
        LONG_TERM_PERIOD).execute();
    return macd.subtract(mSignal.getEMA(macd)).doubleValue();
  }

  private static class Signal {

    final int period;
    final BigDecimal multiplier;
    BigDecimal ema = BigDecimal.ZERO;

    private Signal(int period) {
      this.period = period;
      this.multiplier = new BigDecimal(2 / ((double) period + 1));
    }

    public BigDecimal getEMA(BigDecimal macd) {
      BigDecimal yesterday = new BigDecimal(ema.doubleValue());
      ema = macd.subtract(yesterday).multiply(multiplier).add(yesterday);
      return ema;
    }
  }

  public static class CrossoverObservable implements Observable.OnSubscribe<Histogram> {

    private Histogram mPreviousHistogram = new Histogram();
    private double mCurrentHistogramValue = 0;

    public CrossoverObservable(Histogram previousHistogram, double currentHistogramValue) {
      mPreviousHistogram = previousHistogram;
      mCurrentHistogramValue = currentHistogramValue;
    }

    @Override
    public void call(Subscriber<? super Histogram> subscriber) {
      // uptrend
      Trending trending = mCurrentHistogramValue > 0 ? Trending.UPTREND : Trending.DOWNTREND;
      if (mPreviousHistogram.getTrending() != trending) {
        // crosses over!
        subscriber.onNext(new Histogram(mCurrentHistogramValue, trending));
      } else {
        // maybe getting stronger or weaker?
        Trending t = getAccuracyTrending();
        if (t != null) {
          mPreviousHistogram.setTrending(t);
        }
        // still in trending.
        mPreviousHistogram.setHistogram(mCurrentHistogramValue);
        subscriber.onNext(mPreviousHistogram);
      }
    }

    private Trending getAccuracyTrending() {
      switch (mPreviousHistogram.getTrending()) {
        case DOWNTREND:
        case STRONG_DOWNTREND:
          boolean isStrongDowntrend = Double.compare(mPreviousHistogram.getHistogram(), mCurrentHistogramValue) > 0;
          return isStrongDowntrend ? Trending.STRONG_DOWNTREND : Trending.DOWNTREND;
        case UPTREND:
        case STRONG_UPTREND:
          boolean isStrongUpTrend = Double.compare(mCurrentHistogramValue, mPreviousHistogram.getHistogram()) > 0;
          return isStrongUpTrend ? Trending.STRONG_UPTREND : Trending.UPTREND;
      }
      return null;
    }
  }
}
