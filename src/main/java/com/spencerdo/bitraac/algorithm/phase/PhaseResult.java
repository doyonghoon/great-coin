package com.spencerdo.bitraac.algorithm.phase;

/**
 * Created by doyonghoon on 15. 5. 9..
 *
 * @author doyonghoon
 */
public class PhaseResult {

  private Trending mTrending;
  private double mValue;

  public PhaseResult(Trending trending, double value) {
    mTrending = trending;
    mValue = value;
  }

  public Trending getTrending() {
    return mTrending;
  }

  public double getValue() {
    return mValue;
  }

  @Override public String toString() {
    return "[trending: " + mTrending.name() + ", value: " + mValue + "]";
  }
}
