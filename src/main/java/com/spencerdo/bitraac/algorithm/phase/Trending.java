package com.spencerdo.bitraac.algorithm.phase;

/**
 * Created by doyonghoon on 15. 5. 9..
 */
public enum Trending {
  NONE,
  UPTREND,
  STRONG_UPTREND,
  DOWNTREND,
  STRONG_DOWNTREND
}
