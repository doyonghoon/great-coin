package com.spencerdo.bitraac.algorithm;

import com.xeiam.xchange.bitstamp.service.polling.BitstampMarketDataServiceRaw;
import com.xeiam.xchange.currency.CurrencyPair;
import com.xeiam.xchange.dto.marketdata.Trade;
import com.xeiam.xchange.dto.marketdata.Trades;
import com.xeiam.xchange.service.polling.marketdata.PollingMarketDataService;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * A worker that brings the latest trades from the market at every fixed time,
 * it runs in another thread.
 *
 * @author doyonghoon
 */
public class PollingBitstampMarketDataService {

  public interface OnPollingMarketDataListener {
    void onUpdateTrade(List<Trade> trades) throws IOException;
  }

  private static final int FIXED_SECONDS = 20;
  private final ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
  private final OnPollingMarketDataListener mCallback;
  private final PollingMarketDataService mMarketDataService;

  private ScheduledFuture<?> mBeeperHandle;

  public PollingBitstampMarketDataService(PollingMarketDataService service, OnPollingMarketDataListener callback) {
    mMarketDataService = service;
    mCallback = callback;
  }

  private Runnable mTimer = new Runnable() {
    @Override public void run() {
      if (mCallback != null) {
        try {
          Trades trades = mMarketDataService.getTrades(CurrencyPair.BTC_USD, BitstampMarketDataServiceRaw.BitstampTime.MINUTE);
          mCallback.onUpdateTrade(trades.getTrades());
        } catch (IOException e) {
          e.printStackTrace();
        }
      } else {
        WLog.i("cannot callback, no callback registered.");
      }
    }
  };

  public void start() {
    mBeeperHandle = mScheduler.scheduleAtFixedRate(mTimer, FIXED_SECONDS, FIXED_SECONDS, TimeUnit.SECONDS);
    mScheduler.schedule(new Runnable() {
      public void run() {
        WLog.i("cancel!");
        mBeeperHandle.cancel(true);
      }
    }, 365, TimeUnit.DAYS);
  }
}
