package com.spencerdo.bitraac.algorithm;

import com.spencerdo.bitraac.algorithm.indicators.AroonDown;
import com.spencerdo.bitraac.algorithm.indicators.AroonUp;
import com.spencerdo.bitraac.algorithm.indicators.PPO;
import com.spencerdo.bitraac.algorithm.indicators.ROC;
import com.spencerdo.bitraac.data.ExchangeAccount;
import com.spencerdo.bitraac.data.ExchangeMarket;
import com.xeiam.xchange.dto.Order;
import rx.functions.Action1;

import java.math.BigDecimal;

public class MultiIndicatorsAlgorithm extends TradingAlgorithm {

  private static final int LONG_TERM_EMA_NB_PERIODS = 12;
  private static final int SHORT_TERM_EMA_NB_PERIODS = 4;
  private static final int AROON_NB_PERIODS = 6;
  private static final int ROC_NB_PERIODS = 48;
  private static final double ROC_HIGH_THRESHOLD = 10.0;

  private double mLastRate = 0;

  private static final BigDecimal TRADE_MARGIN = new BigDecimal(0.01);

  public MultiIndicatorsAlgorithm(ExchangeAccount account) {
    super(account);
  }

  @Override
  public void placeOrder(Action1<Order> orderAction, BigDecimal lastPrice) {
  }

//  @Override public Order placeOrder() {
//    Order order = null;
//    if (ExchangeMarket.isEnoughPeriods(ROC_NB_PERIODS)) {
//      if (isOverbought()) {
//        // Overbought
//        BigDecimal btcBalance = getExchangeAccount().getCurrentBtcBalance();
//        btcBalance = btcBalance.subtract(TRADE_MARGIN);
//        if (btcBalance.compareTo(BigDecimal.ZERO) == 1) {
//          order = new MarketOrder(Order.OrderType.ASK, btcBalance, CurrencyPair.BTC_USD);
//        }
//      } else if (isOversold()) {
//        // Oversold
//        BigDecimal lastTradePrice = ExchangeMarket.getPreviousTrades()
//            .get(ExchangeMarket.getPreviousTrades().size() - 1)
//            .getPrice();
//        BigDecimal btcToBeBought = getExchangeAccount().getCurrentUsdBalance()
//            .divide(lastTradePrice, RoundingMode.HALF_UP)
//            .subtract(TRADE_MARGIN);
//        if (btcToBeBought.compareTo(BigDecimal.ZERO) == 1) {
//          order = new MarketOrder(Order.OrderType.BID, btcToBeBought, CurrencyPair.BTC_USD);
//        }
//      } else {
//        double ppo = getTrendStrength();
//        if (new BigDecimal(String.valueOf(ppo)).compareTo(new BigDecimal(String.valueOf(mLastRate)))
//            == 0) {
//          return null;
//        }
//        mLastRate = ppo;
//        if (ppo > 0.5) {
//          // Strong up trend
//          order = new MarketOrder(Order.OrderType.ASK, new BigDecimal("1.0"), CurrencyPair.BTC_USD);
//        } else if (ppo > 0.1) {
//          // Up trend
//          order =
//              new MarketOrder(Order.OrderType.ASK, new BigDecimal("0.25"), CurrencyPair.BTC_USD);
//        } else if (ppo < -0.1) {
//          // Down trend
//          order =
//              new MarketOrder(Order.OrderType.BID, new BigDecimal("0.25"), CurrencyPair.BTC_USD);
//        } else if (ppo < -0.4) {
//          // Strong down trend
//          order = new MarketOrder(Order.OrderType.BID, new BigDecimal("1.0"), CurrencyPair.BTC_USD);
//        } else {
//          // Stability
//          order = null;
//        }
//      }
//    }
//    return order;
//  }

  public double getLastAlgorithmRate() {
    return mLastRate;
  }

  private double getTrendStrength() {
    double ppo = 0;
    if (ExchangeMarket.isEnoughPeriods(LONG_TERM_EMA_NB_PERIODS)) {
      ppo = new PPO(ExchangeMarket.getPreviousPeriods(), SHORT_TERM_EMA_NB_PERIODS,
          LONG_TERM_EMA_NB_PERIODS).execute().doubleValue();
    }
    return ppo;
  }

  private boolean isConsolidation() {
    double aroonUp = new AroonUp(ExchangeMarket.getPreviousPeriods(), AROON_NB_PERIODS).execute();
    double aroonDown =
        new AroonDown(ExchangeMarket.getPreviousPeriods(), AROON_NB_PERIODS).execute();
    return (aroonUp < 50 && aroonDown < 50);
  }

  private boolean isOverbought() {
    return new ROC(ExchangeMarket.getPreviousPeriods(), ROC_NB_PERIODS).execute()
        >= ROC_HIGH_THRESHOLD;
  }

  private boolean isOversold() {
    return new ROC(ExchangeMarket.getPreviousPeriods(), ROC_NB_PERIODS).execute() <= (
        ROC_HIGH_THRESHOLD
            * -1.0);
  }
}
