package com.spencerdo.bitraac.algorithm.indicators;

import com.spencerdo.bitraac.data.Period;
import com.xeiam.xchange.dto.marketdata.Trade;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.Validate;

/**
 *
 * %K = (Current Close - Lowest Low)/(Highest High - Lowest Low) * 100
 * %D = 3-day SMA of %K
 Lowest Low = lowest low for the look-back period
 Highest High = highest high for the look-back period
 %K is multiplied by 100 to move the decimal point two places
 *
 * @author doyonghoon
 */
public class Stochastics implements Indicator<BigDecimal> {

  private ArrayList<Period> periods;

  public Stochastics(final List<Period> periods, int lastPeriods) {
    Validate.noNullElements(periods, "List of periods is null or contains null periods");
    final int nbPeriods = periods.size();
    if (lastPeriods > nbPeriods) {
      throw new IllegalArgumentException("Not enough periods");
    }
    this.periods = new ArrayList<Period>(periods.subList(nbPeriods - lastPeriods, nbPeriods));
  }

  private BigDecimal getSlow() {
    BigDecimal sma = new SMA(periods, 9).execute();
    return sma;
  }

  private BigDecimal getFast() {
    BigDecimal currentClosePrice = periods.get(periods.size() - 1).getLast().getPrice();
    BigDecimal lowestLow = getLowestLow().getPrice();
    BigDecimal highestHigh = getHighestHigh().getPrice();
    BigDecimal first = currentClosePrice.subtract(lowestLow);
    BigDecimal second = highestHigh.subtract(lowestLow);
    return first.divide(second, BigDecimal.ROUND_UP).multiply(IndicatorUtils.HUNDRED);
  }

  private Trade getLowestLow() {
    Trade result = null;
    for (Period period : periods) {
      Trade low = period.getLow();
      if (result == null) {
        result = low;
      } else if (result.getPrice().compareTo(low.getPrice()) < 0) {
        result = low;
      }
    }
    return result;
  }

  private Trade getHighestHigh() {
    Trade result = null;
    for (Period period : periods) {
      Trade high = period.getHigh();
      if (result == null) {
        result = high;
      } else if (result.getPrice().compareTo(high.getPrice()) > 0) {
        result = high;
      }
    }
    return result;
  }

  @Override public BigDecimal execute() {
    return getFast();
  }
}
