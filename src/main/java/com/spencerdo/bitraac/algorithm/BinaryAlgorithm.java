package com.spencerdo.bitraac.algorithm;

import com.spencerdo.bitraac.data.ExchangeAccount;
import com.spencerdo.bitraac.data.ExchangeMarket;
import com.xeiam.xchange.dto.Order;
import com.xeiam.xchange.dto.marketdata.Trade;
import rx.functions.Action1;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class BinaryAlgorithm extends TradingAlgorithm {

    public BinaryAlgorithm(ExchangeAccount account) {
        super(account);
    }

    @Override
    public void placeOrder(Action1<Order> orderAction, BigDecimal lastPrice) {

    }

//    @Override public Order placeOrder() {
//        Order order;
//        double trendCoef = getTrendCoef();
//        if (trendCoef > 1.04) {
//            // Up trend
//            order = new MarketOrder(Order.OrderType.ASK, new BigDecimal(2.2), CurrencyPair.BTC_USD);
//        } else if (trendCoef > 1.015) {
//            // Up trend
//            order = new MarketOrder(Order.OrderType.ASK, new BigDecimal(2), CurrencyPair.BTC_USD);
//        } else if (trendCoef < 0.99) {
//            // Down trend
//            order = new MarketOrder(Order.OrderType.BID, new BigDecimal(2), CurrencyPair.BTC_USD);
//        } else if (trendCoef < 0.97) {
//            // Down trend
//            order = new MarketOrder(Order.OrderType.BID, new BigDecimal(2.2), CurrencyPair.BTC_USD);
//        } else {
//            // Stability
//            order = null;
//        }
//        return order;
//    }

    private double getTrendCoef() {
        double trendCoef = 1.0;
        if (ExchangeMarket.isEnoughTrades(2)) {
            List<Trade> trades = ExchangeMarket.getPreviousTrades();
            BigDecimal previousPrice = trades.get(trades.size() - 2).getPrice();
            BigDecimal lastPrice = trades.get(trades.size() - 1).getPrice();
            trendCoef = previousPrice.divide(lastPrice, 12, RoundingMode.HALF_UP).doubleValue();
        }
        return trendCoef;
    }

}
