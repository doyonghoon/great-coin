package com.spencerdo.bitraac.algorithm;

import com.spencerdo.bitraac.data.DataUtils;
import com.spencerdo.bitraac.data.ExchangeMarket;
import com.xeiam.xchange.dto.marketdata.Trade;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * 정해진 시간마다 과거 데이터를 삭제함.
 */
public class MarketTradeRemover {

  private static final int FIXED_HOURS = 4;
  private final ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
  private ScheduledFuture<?> mBeeperHandle;

  private Runnable mTimer = new Runnable() {
    @Override public void run() {
      int removedItems = DataUtils.removeTradeAfterThreeHours();
      if (removedItems > 0) {
        // 캐시 비움.
        ExchangeMarket.clearPreviousPeriods();
        for (Trade t : DataUtils.getLocalTrades()) {
          ExchangeMarket.addTrade(t);
        }
        WLog.i("removed trade items: " + removedItems + ", reset periods.");
      }
    }
  };

  public void start() {
    mBeeperHandle = mScheduler.scheduleAtFixedRate(mTimer, FIXED_HOURS, FIXED_HOURS, TimeUnit.HOURS);
    mScheduler.schedule(new Runnable() {
      public void run() {
        WLog.i("cancel!");
        mBeeperHandle.cancel(true);
      }
    }, 365, TimeUnit.DAYS);
  }
}
