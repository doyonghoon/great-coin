package com.spencerdo.bitraac.algorithm;

import com.xeiam.xchange.Exchange;
import com.xeiam.xchange.dto.account.AccountInfo;
import com.spencerdo.bitraac.data.ExchangeAccount;
import java.io.IOException;

/**
 * 비트코인 시장에서 거래하는 봇.
 */
public abstract class ExchangeBot {

  public abstract Exchange getExchange();

  public abstract TradingAlgorithm getAlgorithm();

  protected AccountInfo getAccount() {
    try {
      return getExchange().getPollingAccountService().getAccountInfo();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  protected abstract ExchangeAccount getExchangeAccount();

  public abstract void onStart();
}
