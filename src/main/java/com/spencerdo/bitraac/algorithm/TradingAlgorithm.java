package com.spencerdo.bitraac.algorithm;

import com.spencerdo.bitraac.data.ExchangeAccount;
import com.xeiam.xchange.dto.Order;
import rx.functions.Action1;

import java.math.BigDecimal;

/**
 * It constructs the base of algorithm to be used in trading.
 * The key method of the algorithm is placeOrder(). The Bot will observe the value returned from #placeOrder(),
 * whether the bot could create a trade or not.
 *
 * @see #placeOrder(Action1, BigDecimal)
 *
 * @author doyonghoon
 */
public abstract class TradingAlgorithm {

  /** The exchange account of the user */
  private ExchangeAccount exchangeAccount;

  public TradingAlgorithm() {

  }

  public TradingAlgorithm(ExchangeAccount account) {
    this.exchangeAccount = account;
  }

  /**
   * Place an order (bid or ask).
   *
   * @return the placed order
   */
  public abstract void placeOrder(Action1<Order> orderAction, BigDecimal lastPrice);

  /**
   * @return the exchange account of the user
   */
  public ExchangeAccount getExchangeAccount() {
    return exchangeAccount;
  }

  public void setExchangeAccount(ExchangeAccount account) {
    exchangeAccount = account;
  }
}
