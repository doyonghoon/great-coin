package com.spencerdo.bitraac.algorithm;

import com.spencerdo.bitraac.algorithm.indicators.PPO;
import com.spencerdo.bitraac.data.ExchangeAccount;
import com.spencerdo.bitraac.data.ExchangeMarket;
import com.xeiam.xchange.dto.Order;
import rx.functions.Action1;

import java.math.BigDecimal;

public class PPOAlgorithm extends TradingAlgorithm {

  private double mLastRate = 0;

  public PPOAlgorithm(ExchangeAccount account) {
    super(account);
  }

  @Override
  public void placeOrder(Action1<Order> orderAction, BigDecimal lastPrice) {

  }

  public PPOAlgorithm() {
  }

  public double getLastAlgorithmRate() {
    return mLastRate;
  }

//  @Override public Order placeOrder() {
//    Order order;
//    double ppo = getPPO();
//    if (new BigDecimal(String.valueOf(ppo)).compareTo(new BigDecimal(String.valueOf(mLastRate))) == 0) {
//      return null;
//    }
//    mLastRate = ppo;
//    if (ppo > 0.09) {
//      order = new MarketOrder(Order.OrderType.ASK, new BigDecimal("2.0"), CurrencyPair.BTC_USD);
//    } else if (ppo <= -0.07) {
//      order = new MarketOrder(Order.OrderType.BID, new BigDecimal("2.0"), CurrencyPair.BTC_USD);
//    } else {
//      // Stability
//      order = null;
//    }
//    return order;
//  }

  private double getPPO() {
    double ppo = 0;
    if (ExchangeMarket.isEnoughPeriods(26)) {
      ppo = new PPO(ExchangeMarket.getPreviousPeriods(), 12, 26).execute().doubleValue();
    }
    return ppo;
  }
}
