package com.spencerdo.bitraac.algorithm;

import com.spencerdo.bitraac.algorithm.indicators.SMA;
import com.spencerdo.bitraac.data.ExchangeAccount;
import com.spencerdo.bitraac.data.ExchangeMarket;
import com.xeiam.xchange.dto.Order;
import rx.functions.Action1;

import java.math.BigDecimal;

public class SimpleMovingAveragesAlgorithm extends TradingAlgorithm {

    public SimpleMovingAveragesAlgorithm(ExchangeAccount account) {
        super(account);
    }

    @Override
    public void placeOrder(Action1<Order> orderAction, BigDecimal lastPrice) {

    }

//    @Override public Order placeOrder() {
//        Order order;
//        double trendCoef = getTrendCoef();
//        if (trendCoef > 1.02) {
//            // Up trend
//            order = new MarketOrder(Order.OrderType.ASK, new BigDecimal(2), CurrencyPair.BTC_USD);
//        } else if (trendCoef < 0.98) {
//            // Down trend
//            order = new MarketOrder(Order.OrderType.BID, new BigDecimal(2), CurrencyPair.BTC_USD);
//        } else {
//            // Stability
//            order = null;
//        }
//        return order;
//    }

    private double getTrendCoef() {
        double trendCoef = 1.0;
        if (ExchangeMarket.isEnoughPeriods(25)) {
            double movingAvg25 = new SMA(ExchangeMarket.getPreviousPeriods(), 25).execute().doubleValue();
            double movingAvg10 = new SMA(ExchangeMarket.getPreviousPeriods(), 10).execute().doubleValue();
            trendCoef = movingAvg10 / movingAvg25;
        }
        return trendCoef;
    }

}
