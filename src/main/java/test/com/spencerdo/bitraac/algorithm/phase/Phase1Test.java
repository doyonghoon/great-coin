package test.com.spencerdo.bitraac.algorithm.phase;

import com.spencerdo.bitraac.algorithm.phase.Histogram;
import com.spencerdo.bitraac.algorithm.phase.Phase1;
import com.spencerdo.bitraac.algorithm.phase.Trending;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

import java.math.BigDecimal;

/**
 * Phase1 Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>5월 29, 2015</pre>
 */
public class Phase1Test {

  private Phase1 mPhase1 = null;

  @Before
  public void before() throws Exception {
    mPhase1 = new Phase1();
  }

  @After
  public void after() throws Exception {
  }

  /**
   * Method: round(double value, int places)
   */
  @Test
  public void testRound() throws Exception {
//TODO: Test goes here... 
  }

  /**
   * Method: execute(Action1<Order> action, final ExchangeAccount account, final BigDecimal lastPrice)
   */
  @Test
  public void testExecuteForActionAccountLastPrice() throws Exception {
//TODO: Test goes here... 
  }

  /**
   * Method: getMaxCoins(BigDecimal price, BigDecimal usd)
   */
  @Test
  public void testGetMaxCoins() throws Exception {
    BigDecimal maxCoins = Phase1.getMaxCoins(new BigDecimal("235.0"), new BigDecimal("938.22"));
    boolean moreThanZero = maxCoins.compareTo(BigDecimal.ZERO) > 0;
    Assert.assertTrue(moreThanZero);
  }

  @Test
  public void cutDecimalPlacesIfNeeded() throws Exception {
    BigDecimal value1 = new BigDecimal("3.98765432101234567899");
    BigDecimal value2 = new BigDecimal("3.987");
    BigDecimal result1 = mPhase1.trimDecimalPlacesIfNeeded(value1);
    BigDecimal result2 = mPhase1.trimDecimalPlacesIfNeeded(value2);
    boolean moreThanEightDecimalPlaces1 = getNumberOfDecimalPlace(result1) > 8;
    boolean moreThanEightDecimalPlaces2 = getNumberOfDecimalPlace(result2) > 8;
    Assert.assertTrue(!moreThanEightDecimalPlaces1);
    Assert.assertTrue(!moreThanEightDecimalPlaces2);
  }

  int getNumberOfDecimalPlace(BigDecimal number) {
    int scale = number.stripTrailingZeros().scale();
    return scale > 0 ? scale : 0;
  }

  @Test
  public void testFilterHistogram() throws Exception {
    final Func1<Histogram, Boolean> filter =  mPhase1.filterHistogram(new BigDecimal("235.0"));
    final Histogram[] histograms = {
        new Histogram(0.07, Trending.UPTREND),
        new Histogram(-0.05, Trending.DOWNTREND),
        new Histogram(-0.12, Trending.STRONG_DOWNTREND)
    };
    final Action1<Histogram> histogramAction1 = new Action1<Histogram>() {
      @Override
      public void call(Histogram histogram) {
        Assert.assertTrue(Math.abs(histogram.getHistogram()) >= 0.07);
      }
    };
    for (Histogram h : histograms) {
      Observable.just(h).filter(filter).subscribe(histogramAction1);
    }
  }

  /**
   * Method: getEMA(BigDecimal macd)
   */
  @Test
  public void testExecuteMacd() throws Exception {
//TODO: Test goes here... 
  }

  /**
   * Method: call(Subscriber<? super Histogram> subscriber)
   */
  @Test
  public void testCall() throws Exception {
//TODO: Test goes here... 
  }


  /**
   * Method: getHistogramValue()
   */
  @Test
  public void testGetHistogramValue() throws Exception {
//TODO: Test goes here... 
/* 
try { 
   Method method = Phase1.getClass().getMethod("getHistogramValue"); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) { 
} catch(InvocationTargetException e) { 
} 
*/
  }

  /**
   * Method: getAccuracyTrending()
   */
  @Test
  public void testGetAccuracyTrending() throws Exception {
//TODO: Test goes here... 
/* 
try { 
   Method method = Phase1.getClass().getMethod("getAccuracyTrending"); 
   method.setAccessible(true); 
   method.invoke(<Object>, <Parameters>); 
} catch(NoSuchMethodException e) { 
} catch(IllegalAccessException e) {
} catch(InvocationTargetException e) { 
} 
*/
  }

} 
