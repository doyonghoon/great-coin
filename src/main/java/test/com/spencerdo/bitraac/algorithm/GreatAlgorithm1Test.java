package test.com.spencerdo.bitraac.algorithm;

import com.spencerdo.bitraac.algorithm.phase.Histogram;
import com.spencerdo.bitraac.algorithm.phase.Phase1;
import com.spencerdo.bitraac.algorithm.phase.Trending;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import rx.Observable;

/**
 * GreatAlgorithm1 Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>5월 27, 2015</pre>
 */
public class GreatAlgorithm1Test {

  private static final Histogram INITIAL_HISTOGRAM = new Histogram(0.05, Trending.UPTREND);
  private static final Histogram RESULT_HISTOGRAM = new Histogram(0.07, Trending.STRONG_UPTREND);

  Phase1.CrossoverObservable mCrossoverObservable = null;

  @Before
  public void before() throws Exception {
    mCrossoverObservable = new Phase1.CrossoverObservable(INITIAL_HISTOGRAM, RESULT_HISTOGRAM.getHistogram());
  }

  @After
  public void after() throws Exception {

  }

  /**
   * Method: placeOrder(Action1<Order> orderAction, BigDecimal lastPrice)
   */
  @Test
  public void testPlaceOrder() throws Exception {
    Observable.create(mCrossoverObservable).subscribe(histogram -> {
      Assert.assertEquals(histogram, RESULT_HISTOGRAM);
    });
  }
}
